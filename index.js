//console.log("Test connection");

// ACTIVITY 1
let number = parseInt(prompt("Please type a number:"));

console.log("The number you provided is " + number + ".");

for (number; number >= 0; number--){
	if (number == 50) {
		console.log("The current value is at 50. Terminating the loop.")
		break;
	} else if ((number % 10) == 0 && number != 0){
		console.log("The number is divisible by 10. Skipping the number.");
	} else if ((number % 5) == 0  && number != 0){
		console.log(number);
	}
}

// ACTIVITY 2
let word="supercalifragilisticexpialidocious";
let consonants="";


for (let x=0; x < word.length; x++){
	
	if ( word[x] == "a" || word[x] == "e" || word[x] == "i" || word[x] == "o" || word[x] == "u" ) {
		continue;	
	} else {
		consonants +=word[x];

	}
}
console.log(word);
console.log(consonants);

